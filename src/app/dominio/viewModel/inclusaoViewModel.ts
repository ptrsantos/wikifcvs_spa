import { Artigo } from "../model/artigo";
import { Secao } from "../model/secao";
import { Tema } from "../model/tema";

export class InclusaoViewModel {
    secao: Secao;
    tema: Tema;
    artigo: Artigo;
}