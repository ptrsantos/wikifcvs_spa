import { Tema } from "./tema"

export class Secao {
    id: string
    titulo: string
    temas: Array<Tema>
    autor: string

    // constructor(data: Partial<Secao>){
    //     Object.assign({}, this, data)
    // }
}