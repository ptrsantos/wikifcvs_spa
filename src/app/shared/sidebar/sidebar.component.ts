import { Secao } from 'src/app/dominio/model/secao';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { routes } from '../../consts/routes';
import { SharedService } from '../services/shared.service';
import { Tema } from 'src/app/dominio/model/tema';
import { Artigo } from 'src/app/dominio/model/artigo';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{

  public routes: typeof routes = routes;
  public isOpenUiElements = false;
  erros: any[] = new Array<any>();
  Secoes = new Array<Secao>();
  secao: Secao; 

  constructor(private sharedService: SharedService,
              private router: Router,
              private toastrService: ToastrService,
              private spinnerService: NgxSpinnerService){}

  public openUiElements() {
    this.isOpenUiElements = !this.isOpenUiElements;
  }

  ngOnInit(): void {
    this.listarDados()
  }

  listarDados(){
    this.sharedService.listarDaddos().subscribe(
      sucesso => {this.processarSucesso(sucesso)},
      falha => {this.processarFalha(falha)}
    )
  }

  processarSucesso(response) {
    debugger
    this.erros = [];
    let toastr = this.toastrService.success("Registro realizado com sucesso.", "Parabéns: ")
    if (toastr) {
      toastr.onHidden.subscribe(() => {
        let retorno = JSON.parse(JSON.stringify(response.data))
        if(retorno.length > 0){
          for(let item of response.data){
            debugger
            this.secao = new Secao();
            let itemJson = JSON.stringify(item)
            this.secao = Object.assign({}, new Secao(), JSON.parse(itemJson));
            this.Secoes.push(this.secao)
          }
        }
        // let retorno = JSON.parse(JSON.stringify(response.data))
        // let secoes = retorno.map(secao => {
        //   new Secao({
        //     id: secao.id,
        //     titulo: secao.titulo,
        //     temas: secao.temas ? secao.temas.map(tema => {
        //       new Tema({
        //         id: tema.id,
        //         titulo: tema.titulo,
        //         artigos: tema.artigos ? tema.artigos.map(artigo => {
        //           new Artigo({
        //             id: artigo.id,
        //             titulo: artigo.titulo,
        //             descricao: artigo.descricao
        //           })
        //         }) : []
        //       })
        //     })  : []
        //   })
        // })
        debugger
        console.log(this.Secoes)
        console.log(response)
      })
    }
  }

  // retornaSecao(dado: any){
  //   let secao: Secao = new Secao();
  //   secao.titulo = dado.titulo;
  //   listaTemas: Tema[] = retornaTemas(dado)
  // }

  processarFalha(fails: any){
    debugger
    //this.spinnerService.hide();
    this.erros = fails.error.errors;
    this.toastrService.error(this.erros.toString(), "Ocorreu um erro:")
  }

}
