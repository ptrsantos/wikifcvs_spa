import { Injectable, Injector } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor,  HttpRequest} from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { AuthService } from 'src/app/pages/auth/services';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  private authService: AuthService;
  constructor(private injector: Injector, private router: Router) { }

  // intercept(req, next) {
  //   let authService: AuthService = this.injector.get(AuthService)
  //   let tokenizedReq = req.clone({
  //     setHeaders: {
  //       Authorization: `Bearer ${authService.localStorage.obterTokenUsuario()}`
  //     }
  //   })
  //   console.log()
  //   alert("TokeInterceptorService")
  //   debugger
  //   return next.handle(tokenizedReq)
  // }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.authService = this.injector.get(AuthService)
    if (this.authService.localStorage.obterTokenUsuario() && !req.headers.has('nao_incluir_token')){
      req = this.clonarHeaderInserindoToken(req);
    }
      return next.handle(req).pipe(

        retry(2),

        catchError(error => {

          if(error instanceof HttpErrorResponse){

            if(error.status === 401){
              this.authService.localStorage.limparDadosLocaisUsuario();
              this.router.navigate(['/usuario-sem-autenticacao'])
            }
            if(error.status === 403){
              this.authService.localStorage.limparDadosLocaisUsuario();
              this.router.navigate(['/acesso-negado'])
            }
            if(error.status === 404){
              //this.authService.localStorage.limparDadosLocaisUsuario();
              this.router.navigate(['/not-found'])
            }

            return throwError(error)

          }

        })

      );
  }

  private clonarHeaderInserindoToken(req: HttpRequest<any>) {
     const token = this.authService.localStorage.obterTokenUsuario();
    return req.clone({
      setHeaders: {
        'Authorization': `Bearer ${token}`
      }
    });
  }
}
