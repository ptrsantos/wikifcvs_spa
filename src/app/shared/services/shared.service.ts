import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {TokenUtilService} from '../../pages/auth/services/token-util.service'

const headersSemAuth = new HttpHeaders({ 'nao_incluir_token': 'true', 'Content-Type': 'application/json' })
const headers = new HttpHeaders({ 'Content-Type': 'application/json' })

@Injectable({
  providedIn: 'root'
})
export class SharedService  extends TokenUtilService{

  constructor(private http: HttpClient) {
    super();
  }

  listarDaddos(): Observable<any>{
    //return this.http.post<any>(`${environment.ApiControleUrl}secoes/SalvarDados/`, inclusao, { headers: headersSemAuth});
    return this.http.get<any>(`${environment.ApiControleUrl}secoes/ListarDados/`, { headers: headers});
    
  }
}
