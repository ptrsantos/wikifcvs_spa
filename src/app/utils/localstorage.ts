export class LocalStorageUtils {
   
    public obterUsuario() {
        return JSON.parse(localStorage.getItem('usuario'));
    }

    public salvarDadosLocaisUsuario(response: any){
        // debugger
        // let testeLocalStorage = window.localStorage;
        // let testeLocalStorage2 = localStorage;
        this.salvarTokenUsuario(response.data.accessToken);
        this.salvarUsuario(response.data.userToken);
    }

    public limparDadosLocaisUsuario(){
        localStorage.removeItem('token');
        localStorage.removeItem('usuario')
    }

    public obterTokenUsuario(): string{
        return localStorage.getItem('token');
    }

    public salvarTokenUsuario(token: string){
        localStorage.setItem('token', token)
    }

    public salvarUsuario(user: string){
        localStorage.setItem('usuario', JSON.stringify(user));
    }
}