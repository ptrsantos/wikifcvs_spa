import { Component, OnInit } from '@angular/core';
import { routes } from 'src/app/consts';

@Component({
  selector: 'app-acceso-negado',
  templateUrl: './acceso-negado.component.html',
  styleUrls: ['./acceso-negado.component.scss']
})
export class AccesoNegadoComponent{

  public routes: typeof routes = routes;

}
