import { MaterialModule } from './../../utils/angular-material/material.module';
import { BusinessModule } from './../../business/business.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { NgxEchartsModule } from 'ngx-echarts';
import { TrendModule } from 'ngx-trend';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FormsModule } from '@angular/forms';

import { DashboardPageComponent } from './containers';
import {
  VisitsChartComponent,
  RevenueChartComponent,
  SupportRequestsComponent,
} from './components';
import { SharedModule } from '../../shared/shared.module';
import { DashboardService } from './services';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { DashboardRoutingModule } from './dashoboard-routing.module';


@NgModule({
  declarations: [
    DashboardPageComponent,
    VisitsChartComponent,
    RevenueChartComponent,
    SupportRequestsComponent,
  ],
  imports: [
    CommonModule,
    NgxEchartsModule,
    TrendModule,
    MaterialModule,
    NgApexchartsModule,
    FormsModule,
    SharedModule,
    BusinessModule,
    NgxSpinnerModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      progressBar: true,
      maxOpened: 2,
      autoDismiss: true,
      closeButton: true,
      enableHtml: true,
    }),
    DashboardRoutingModule
  ],
  exports: [
  ],
  providers: [
    DashboardService
  ]
})
export class DashboardModule { }
