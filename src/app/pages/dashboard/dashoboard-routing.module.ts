import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarArtigoComponent } from 'src/app/business/editar-artigo/editar-artigo.component';
import { AuthGuard } from '../auth/guards';


const routes: Routes = [
    { path: 'Home', component: EditarArtigoComponent, canActivate: [AuthGuard], data: { title: 'Home' } },
    { path: '', pathMatch: 'full', redirectTo: 'Home' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
