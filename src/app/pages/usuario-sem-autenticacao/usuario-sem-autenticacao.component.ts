import { Component, OnInit } from '@angular/core';
import { routes } from 'src/app/consts';

@Component({
  selector: 'app-usuario-sem-autenticacao',
  templateUrl: './usuario-sem-autenticacao.component.html',
  styleUrls: ['./usuario-sem-autenticacao.component.scss']
})
export class UsuarioSemAutenticacaoComponent {

  public routes: typeof routes = routes;

}
