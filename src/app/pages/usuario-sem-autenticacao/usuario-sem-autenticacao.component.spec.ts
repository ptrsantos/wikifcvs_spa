import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioSemAutenticacaoComponent } from './usuario-sem-autenticacao.component';

describe('UsuarioSemAutenticacaoComponent', () => {
  let component: UsuarioSemAutenticacaoComponent;
  let fixture: ComponentFixture<UsuarioSemAutenticacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuarioSemAutenticacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioSemAutenticacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
