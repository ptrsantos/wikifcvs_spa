import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services';
import { routes } from '../../../../consts';
import { Usuario } from 'src/app/dominio/model/usuario';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent {
  public todayDate: Date = new Date();
  public routers: typeof routes = routes;
  erros: any[] = new Array<any>();

  constructor(
    private service: AuthService,
    private router: Router,
    private toastrService: ToastrService,
    private spinnerService: NgxSpinnerService
  ) { }

  public sendLoginForm(usuario: Usuario): void {
    this.spinnerService.show();
    this.service.registrarLogin(usuario).subscribe(
        sucesso => {this.processarSucessoLogin(sucesso)},
        falha => {this.processarFalharLogin(falha)}
      )
  }

  processarSucessoLogin(response) {
    this.service.localStorage.salvarDadosLocaisUsuario(response)
    this.erros = [];
    let toastr = this.toastrService.success("Registro realizado com sucesso.", "Parabéns: ")
    if (toastr) {
      toastr.onHidden.subscribe(() => {
        this.spinnerService.hide();
        this.router.navigate([this.routers.DASHBOARD]).then();
        //this.router.navigate(['/dashboard']);
      })
    }
  }

  processarFalharLogin(fails: any){
    this.spinnerService.hide();
    this.erros = fails.error.errors;
    this.toastrService.error(this.erros.toString(), "Ocorreu um erro:")
  }

  public sendSignForm(usuario: Usuario): void {
    //this.service.sign();
    this.service.registrarUsuario(usuario).subscribe(
        sucesso => {this.processarSucessoSign(sucesso)},
        falha => {this.processarFalharSign(falha)}
      )
    
  }

  processarSucessoSign(response: any){
    this.service.localStorage.salvarDadosLocaisUsuario(response);
    this.erros = [];
    let resposta = response;
    let toastr = this.toastrService.success("Registro realizado com sucesso.", "Parabéns: ")
    if(toastr){
      toastr.onHidden.subscribe(() => {
        this.spinnerService.hide()
        this.router.navigate(['/dashboard']);
        //this.router.navigate([this.routers.DASHBOARD]).then();
      })
    }
  }

  processarFalharSign(fails: any){
    this.spinnerService.hide()
    this.erros = fails.error.errors;
    this.toastrService.error(this.erros.toString(), "Ocorreu um erro:")
  }

}
