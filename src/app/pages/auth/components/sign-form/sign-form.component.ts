import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/dominio/model/usuario';

@Component({
  selector: 'app-sign-form',
  templateUrl: './sign-form.component.html',
  styleUrls: ['./sign-form.component.scss']
})
export class SignFormComponent implements OnInit {

  @Output() sendSignForm = new EventEmitter<Usuario>();
  public form: FormGroup;
  usuario: Usuario;
  //erros: any[] = new Array<any>();

  constructor(){}

  public ngOnInit(): void {
    this.form = new FormGroup({
      // name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      confirmpassword: new FormControl('', [Validators.required])
    });
  }

  public registrarUsuario(): void {
    if (this.form.valid) {
      //this.sendSignForm.emit();
      // this.authService.registrarUsuario(this.usuario).subscribe(
      //   sucesso => {this.processarSucesso(sucesso)},
      //   falha => {this.processarFalhar(falha)}
        
      //   )
        this.usuario = Object.assign({}, this.usuario, this.form.value)
        this.sendSignForm.emit(this.usuario)
    }
  }

  // processarSucesso(response: any){
  //   this.form.reset();
  //   //this.erros = [];
  //   //new LocalStorageUtils().salvarDadosLocaisUsuario(response);
  //   let resposta = response;
  //   this.authService.localStorage.salvarDadosLocaisUsuario(response);
  //   let toastr = this.toastrService.success("Registro realizado com sucesso.", "Parabéns: ")
  //   if(toastr){
  //     toastr.onHidden.subscribe(() => {
  //       this.router.navigate(['/dashboard']);
  //       this.spinnerService.hide()
  //     })
  //   }
  // }

  // processarFalhar(fails: any){
  //   this.spinnerService.hide()
  //   this.erros = fails.error.errors;
  //   this.toastrService.error(this.erros.toString(), "Ocorreu um erro:")
  // }


}
