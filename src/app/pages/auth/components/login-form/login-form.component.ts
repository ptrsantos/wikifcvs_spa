import { AuthService } from './../../services/auth.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/dominio/model/usuario';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output() sendLoginForm = new EventEmitter<Usuario>();
  public form: FormGroup;
  erros: any[] = new Array<any>();
  usuario: Usuario;


  constructor() { }  

  public ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
    //this.form.reset()
  }

  registrarLogin(){
    // this.authService.registrarLogin(this.usuario).subscribe(
    //   sucesso => {this.processarSucesso(sucesso)},
    //   falha => {this.processarFalhar(falha)}
    //   )
      //this.mudancasNaoSalvas = false;
      this.usuario = Object.assign({}, this.usuario, this.form.value);

      this.sendLoginForm.emit(this.usuario)
  }

  // processarSucesso(response) {
  //   this.form.reset();
  //   this.erros = [];
  //   // debugger
  //   // let resposta = response
  //   // this.authService.localStorage.salvarDadosLocaisUsuario(response);
  //   let toastr = this.toastrService.success("Registro realizado com sucesso.", "Parabéns: ")
  //   if (toastr) {
  //     toastr.onHidden.subscribe(() => {
  //       this.spinnerService.hide();
  //       this.router.navigate(['/dashboard']);
  //     })
  //   }
  // }
  
  // processarFalhar(fails: any){
  //   this.spinnerService.hide();
  //   this.erros = fails.error.errors;
  //   this.toastrService.error(this.erros.toString(), "Ocorreu um erro:")
  // }
}
