import { Injectable } from '@angular/core';
import { LocalStorageUtils } from 'src/app/utils/localstorage';

@Injectable({
  providedIn: 'root'
})
export class TokenUtilService {

  localStorage: LocalStorageUtils;
  constructor() { 
    this.localStorage = new LocalStorageUtils();
  }

  limparaDadosUsuario(){
    this.localStorage.limparDadosLocaisUsuario()
  }

  retornaEmailUsuario(){
    let usuario = this.localStorage.obterUsuario();
    return usuario.email
  }

  retornaUsuarioId(){
    let usuario = this.localStorage.obterUsuario();
    return usuario.id
  }

}
