import { LocalStorageUtils } from './../../../utils/localstorage';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Usuario } from 'src/app/dominio/model/usuario';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';

import { User } from '../models';
import { TokenUtilService } from './token-util.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends TokenUtilService{

   constructor(private http: HttpClient) {
     super();
   }

  registrarUsuario(usuario: Usuario): Observable<Usuario> {
    let response = this.http.post<Usuario>(environment.ApiControleUrl + 'auth/cadastrar', usuario)
      .pipe(
        // map(this.extractData),
        // catchError(this.serviceError)
      )
    return response;
  }

  registrarLogin(usuario: Usuario){
    let response = this.http.post<Usuario>(environment.ApiControleUrl  + 'auth/entrar', usuario)
    .pipe(
      // map(this.extractData),
      // catchError(this.serviceError)
    )
  return response;
  }


}
