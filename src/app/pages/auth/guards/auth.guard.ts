import { LocalStorageUtils } from './../../../utils/localstorage';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

import { routes } from '../../../consts';

@Injectable()
export class AuthGuard implements CanActivate{
  public routers: typeof routes = routes;
  localStorageUtils: LocalStorageUtils;
  constructor(private router: Router) {
    this.localStorageUtils = new LocalStorageUtils();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    //const token = localStorage.getItem('token');
    const token = this.localStorageUtils.obterTokenUsuario();

    if (token) {
      return true;
    } else {
      this.router.navigate([this.routers.LOGIN]);
    }
  }
}
