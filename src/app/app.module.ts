import { UsuarioSemAutenticacaoComponent } from './pages/usuario-sem-autenticacao/usuario-sem-autenticacao.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AuthModule } from './pages/auth/auth.module';
import { NgxSpinnerModule } from 'ngx-spinner';

import { ToastrModule } from 'ngx-toastr';
import { BusinessModule } from './business/business.module';
import { MaterialModule } from './utils/angular-material/material.module';
import { TokenInterceptorService } from './shared/interceptor/token-interceptor.service';
import { AccesoNegadoComponent } from './pages/acceso-negado/acceso-negado.component';





@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    AccesoNegadoComponent,
    UsuarioSemAutenticacaoComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AuthModule,
    DashboardModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,

    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      progressBar: true
    }),
    BusinessModule,
    MaterialModule,
    NgxSpinnerModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
