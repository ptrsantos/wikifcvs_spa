import { BusinessService } from './../business.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { SecaoComponent } from '../secao/secao.component';
import { TemaComponent } from '../tema/tema.component';
import { ArtigoComponent } from '../artigo/artigo.component';
import { CkeditorComponent } from '../ckeditor/ckeditor.component';
import { Secao } from 'src/app/dominio/model/secao';
import { Tema } from 'src/app/dominio/model/tema';
import { Artigo } from 'src/app/dominio/model/artigo';
import { InclusaoViewModel } from 'src/app/dominio/viewModel/inclusaoViewModel';

@Component({
  selector: 'app-editar-artigo',
  templateUrl: './editar-artigo.component.html',
  styleUrls: ['./editar-artigo.component.css']
})
export class EditarArtigoComponent implements OnInit {

  ocultarEditor: boolean = true;
  form: FormGroup
  secao: Secao;
  tema: Tema;
  artigo: Artigo
  inclusao: InclusaoViewModel;
  @ViewChild(SecaoComponent) secaoViewChild: SecaoComponent
  @ViewChild(TemaComponent) temaViewChild: TemaComponent
  @ViewChild(ArtigoComponent) artigoViewChild: ArtigoComponent
  @ViewChild(CkeditorComponent) ckeditorViewChild: CkeditorComponent

  constructor(private formBuilder: FormBuilder,
              private businessService: BusinessService) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({

    })
  }

  exibirEditorArigo(event){
    //debugger
    this.criarSecao(this.secaoViewChild.secaoFormControl.value);
    this.criarTema(this.temaViewChild.temaFormControl.value);
    //this.criarArtigo(this.artigoViewChild.tituloArtigoFormControl.value, this.temaViewChild.temaFormControl.value)
    this.criarArtigo(this.artigoViewChild)
    this.ocultarEditor = false
  }

  criarSecao(tituloSecao: string){
    this.secao = new Secao();
    this.secao.titulo = tituloSecao;
  }

  criarTema(tituloTema: string){
    this.tema = new Tema();
    this.tema.titulo = tituloTema;
  }

  criarArtigo(aritgoForm: ArtigoComponent){
    //debugger
    // let titulo = aritgoForm.tituloArtigoFormControl.value
    // let descricao = aritgoForm.descricaoArtigoFormControl.value
    // let artigoDescricao = aritgoForm.descricaoFilteredOptions
    this.artigo = new Artigo();
    this.artigo.titulo = aritgoForm.tituloArtigoFormControl.value;
    this.artigo.descricao = aritgoForm.tituloArtigoFormControl.value;
    console.log(this.artigo)
  }

  salvarEdicao(){
    //debugger
    // let corpoDoArtigo = this.ckeditorViewChild.ckEditorForm.value
    this.artigo.conteudo = this.ckeditorViewChild.ckEditorForm.controls.conteudo.value; //"Paulo";
    console.log(this.secao)
    console.log(this.tema)
    console.log(this.artigo)
    this.inclusao = new InclusaoViewModel();
    this.inclusao.secao = this.secao;
    this.inclusao.tema = this.tema;
    this.inclusao.artigo = this.artigo;
    this.businessService.salvarDados(this.inclusao).subscribe(dados => {
      // debugger
      // console.log(dados)
    });
    // this.businessService.salvarDados2(this.secao, this.tema, this.artigo).subscribe(dados => {
    //   debugger
    //   console.log(dados)
    // });
    //this.spinnerService.show();
  }

   

}
