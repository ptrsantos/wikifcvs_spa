import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InclusaoViewModel } from '../dominio/viewModel/inclusaoViewModel';
import { environment } from 'src/environments/environment';

const headersSemAuth = new HttpHeaders({ 'nao_incluir_token': 'true', 'Content-Type': 'application/json' })
const headers = new HttpHeaders({ 'Content-Type': 'application/json' })


@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  constructor(private http: HttpClient) { }

  salvarDados(inclusao: InclusaoViewModel): Observable<any>{
    //return this.http.post<any>(`${environment.ApiControleUrl}secoes/SalvarDados/`, inclusao, { headers: headersSemAuth});
    return this.http.post<any>(`${environment.ApiControleUrl}secoes/SalvarDados/`, inclusao, { headers: headers});
    
  }
}
