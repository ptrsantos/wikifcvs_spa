import { FormGroup, FormControl } from '@angular/forms';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {decode} from 'html-entities';
import { Artigo } from 'src/app/dominio/model/artigo';



@Component({
  selector: 'app-ckeditor',
  templateUrl: './ckeditor.component.html',
  styleUrls: ['./ckeditor.component.css']
})
export class CkeditorComponent implements OnInit {

  ckEditorForm: FormGroup
  name = 'ng2-ckeditor';
  ckeConfig: any;
  ckEditorConteudo: string;
  log: string = '';
  @ViewChild("myckeditor") ckeditor: any;

  public model = {
    editorData: ''
  };

  constructor() {
    this.ckEditorConteudo = "";
   }

  ngOnInit(): void {
    this.ckeConfig = {
      removePlugins: 'undo'
    };

    this.ckEditorForm = new FormGroup({
      conteudo: new FormControl(this.ckEditorConteudo, [])
    })
  }
  
  onEditorChange(event){
    console.log(event)
  }

  onChange(event: any): void {
    // console.log(event);
    // console.log(this.ckEditorConteudo)
    //this.log += new Date() + "<br />";
  }


}
