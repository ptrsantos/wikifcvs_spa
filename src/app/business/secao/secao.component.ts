import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-secao',
  templateUrl: './secao.component.html',
  styleUrls: ['./secao.component.scss']
})
export class SecaoComponent implements OnInit {

  //@Input() formularioPai: FormGroup
  options: string[] = ['Recursos Humanos', 'Análise', 'Preparo', "Análise documental"]
  objectOptions = [
    {name: 'Recursos Humanos'},
    {name: 'Análise documental'},
    {name: 'Jurídico'},
    {name: 'Preparo'},
  ]
  @Input() secaoFormControl = new FormControl();
  filteredOptions: Observable<string[]>

  constructor() { }

  ngOnInit(): void {
    this.filteredOptions = this.secaoFormControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    )
  }

  private _filter(value: string): any{
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue))
    // let retornoOptions: any = this.objectOptions.filter(option => option.name.toLowerCase().includes(value)) 
    // return retornoOptions;
  }

  displayName(subject){
    return subject ? subject.name : undefined
  }

}
