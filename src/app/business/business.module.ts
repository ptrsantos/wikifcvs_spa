import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BusinessRoutingModule } from './business-routing.module';
import { CkeditorComponent } from './ckeditor/ckeditor.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { MaterialModule } from '../utils/angular-material/material.module';
import { EditarArtigoComponent } from './editar-artigo/editar-artigo.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { SecaoComponent } from './secao/secao.component';
import { TemaComponent } from './tema/tema.component';
import { ArtigoComponent } from './artigo/artigo.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BusinessService } from './business.service';


@NgModule({
  declarations: [
    EditarArtigoComponent,
    CkeditorComponent,
    SecaoComponent,
    TemaComponent,
    ArtigoComponent
  ],
  exports: [
    EditarArtigoComponent,
    CkeditorComponent,
    SecaoComponent,
    TemaComponent,
    ArtigoComponent
  ],
  imports: [
    CommonModule,
    BusinessRoutingModule,
    CKEditorModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      progressBar: true,
      maxOpened: 2,
      autoDismiss: true,
      closeButton: true,
      enableHtml: true,
    }),
    NgxSpinnerModule
  ],
  providers:[
    BusinessService
  ]
})
export class BusinessModule { }
